package com.odigeo.membership.mock;

import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.Money;
import com.odigeo.membership.response.PrimeBookingInformation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MemberSubscriptionDetailsBuilderTest {

    private static final String EUR = "EUR";
    private static final String ONE_THOUSAND = "1000";
    private MemberSubscriptionDetailsBuilder membersSubscriptionDetailsBuilder;
    private Money totalSavings;

    @BeforeMethod
    public void setUp() throws Exception {
        MoneyBuilder moneyBuilder = new MoneyBuilder(new Random()).setCurrency(EUR)
                .setAmount(new BigDecimal(ONE_THOUSAND));
        totalSavings = moneyBuilder.build();
        membersSubscriptionDetailsBuilder = new MemberSubscriptionDetailsBuilder(new Random());
    }

    @Test
    public void testBuildWithoutValues() throws IllegalAccessException {
        MemberSubscriptionDetails memberSubscriptionDetails = membersSubscriptionDetailsBuilder.build();
        assertTrue(notNullFields(memberSubscriptionDetails));
    }

    @Test
    public void testBuildWithValues() throws IllegalAccessException {
        createInitializedBuilder();
        MemberSubscriptionDetails memberSubscriptionDetails = membersSubscriptionDetailsBuilder.build();
        assertTrue(notNullFields(memberSubscriptionDetails));
        checkBuilderValues(memberSubscriptionDetails);
    }

    private void checkBuilderValues(MemberSubscriptionDetails memberSubscriptionDetails) {
        assertEquals(memberSubscriptionDetails.getTotalSavings(), membersSubscriptionDetailsBuilder.getTotalSavings());
        assertEquals(memberSubscriptionDetails.getMembershipId(), membersSubscriptionDetailsBuilder.getMembershipId());
    }

    private void createInitializedBuilder() {
        membersSubscriptionDetailsBuilder.setMembershipId(Long.MAX_VALUE);
        membersSubscriptionDetailsBuilder.setPrimeBookingsInfo(Collections.singletonList(new PrimeBookingInformation()));
        membersSubscriptionDetailsBuilder.setTotalSavings(totalSavings);
    }

    private boolean notNullFields(MemberSubscriptionDetails memberSubscriptionDetails) throws IllegalAccessException {
        for (Field field : memberSubscriptionDetails.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.get(memberSubscriptionDetails) == null) {
                return false;
            }
        }
        return true;
    }
}

package com.odigeo.membership.mock;

import com.odigeo.membership.response.Money;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MoneyBuilderTest {
    private static final String EUR = "EUR";
    private MoneyBuilder moneyBuilder;

    @BeforeMethod
    public void setUp() throws Exception {
        moneyBuilder = new MoneyBuilder(new Random());
    }

    @Test
    public void testBuildWithValues() throws Exception {
        moneyBuilder.setAmount(BigDecimal.TEN);
        moneyBuilder.setCurrency(EUR);
        Money money = moneyBuilder.build();
        assertEquals(money.getAmount(), moneyBuilder.getAmount());
        assertEquals(money.getCurrency(), moneyBuilder.getCurrency());
    }

    @Test
    public void testBuildWithoutValues() throws Exception {
        Money money = moneyBuilder.build();
        assertNotNull(money.getAmount());
        assertNotNull(money.getCurrency());
    }
}

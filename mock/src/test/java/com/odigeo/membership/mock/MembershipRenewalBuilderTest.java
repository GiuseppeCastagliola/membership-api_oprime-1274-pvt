package com.odigeo.membership.mock;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MembershipRenewalBuilderTest {

    private static final String RENEWAL_ENABLED = "ENABLED";
    private MembershipRenewalBuilder membershipRenewalBuilder;

    @BeforeMethod
    public void setUp() throws Exception {
        membershipRenewalBuilder = new MembershipRenewalBuilder(new Random());
    }

    @Test
    public void testBuildWithoutValues() throws IllegalAccessException {
        assertNotNull(membershipRenewalBuilder.build().getRenewalStatus());
    }

    @Test
    public void testBuildWithValues() throws IllegalAccessException {
        membershipRenewalBuilder.setRenewalStatus(RENEWAL_ENABLED);
        assertEquals(membershipRenewalBuilder.build().getRenewalStatus(), membershipRenewalBuilder.getRenewalStatus());
    }

}

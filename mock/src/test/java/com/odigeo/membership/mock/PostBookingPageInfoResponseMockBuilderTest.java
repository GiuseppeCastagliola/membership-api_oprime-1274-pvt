package com.odigeo.membership.mock;

import com.odigeo.membership.response.PostBookingPageInfoResponse;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PostBookingPageInfoResponseMockBuilderTest {

    private static final long BOOKING_ID = 1991L;
    private static final String EMAIL = "email";

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testBuildWithoutValues() {
        PostBookingPageInfoResponse postBookingPageInfoResponse = new PostBookingPageInfoResponseMockBuilder(new Random())
                .build();
    }

    @Test
    public void testBuildWithFilledValues() {
        PostBookingPageInfoResponse postBookingPageInfoResponse = new PostBookingPageInfoResponseMockBuilder(new Random())
                .setBookingId(BOOKING_ID)
                .setEmail(EMAIL).build();
        assertEquals(postBookingPageInfoResponse.getBookingId(), BOOKING_ID);
        assertEquals(postBookingPageInfoResponse.getEmail(), EMAIL);
    }

    @Test
    public void testBuildWithEmail() {
        PostBookingPageInfoResponse postBookingPageInfoResponse = new PostBookingPageInfoResponseMockBuilder(new Random())
                .setEmail(EMAIL).build();
        assertTrue(postBookingPageInfoResponse.getBookingId() > 0);
        assertEquals(postBookingPageInfoResponse.getEmail(), EMAIL);
    }

}

package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipWarning;
import com.odigeo.membership.util.PickerUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.google.common.base.MoreObjects.firstNonNull;

public class MembershipBuilder {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private final Picker picker;
    private final StringPicker stringPicker;
    private String name;
    private String lastNames;
    private String website;
    @Deprecated
    private long memberId;
    private long memberAccountId;
    private long membershipId;
    private List<MembershipWarning> warnings;
    private String autoRenewalStatus;
    private String activationDate;
    private String expirationDate;
    private BigDecimal balance;
    private String sourceType;
    private long bookingIdSubscription;
    private String membershipType;
    private int monthsDuration;


    public MembershipBuilder(Random random) {
        this.picker = new Picker(random);
        this.stringPicker = new StringPicker(random);
    }

    public String getName() {
        return name;
    }

    public MembershipBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastNames() {
        return lastNames;
    }

    public MembershipBuilder setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public long getMemberId() {
        return memberId;
    }

    public MembershipBuilder setMemberId(long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public List<MembershipWarning> getWarnings() {
        return warnings;
    }

    public MembershipBuilder setWarnings(List<MembershipWarning> warnings) {
        this.warnings = warnings;
        return this;
    }

    public String getAutoRenewalStatus() {
        return autoRenewalStatus;
    }

    public MembershipBuilder setAutoRenewalStatus(String autoRenewalStatus) {
        this.autoRenewalStatus = autoRenewalStatus;
        return this;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public MembershipBuilder setActivationDate(String activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public MembershipBuilder setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipBuilder setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public MembershipBuilder setMembershipId(long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public long getBookingIdSubscription() {
        return bookingIdSubscription;
    }

    public MembershipBuilder setBookingIdSubscription(long bookingIdSubscription) {
        this.bookingIdSubscription = bookingIdSubscription;
        return this;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public MembershipBuilder setMembershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public String getSourceType() {
        return sourceType;
    }

    public MembershipBuilder setSourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public MembershipBuilder setMonthsDuration(int monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public Membership build() {
        if (isNothingSet()) {
            return null;
        }
        fillMissingInfo();
        return buildMembership();
    }

    private boolean isNothingSet() {
        if (memberId != 0 || memberAccountId != 0 || membershipId != 0 || bookingIdSubscription != 0) {
            return false;
        }
        List<Object> fields = Arrays.asList(name, lastNames, website, warnings, autoRenewalStatus, expirationDate, activationDate, membershipType, sourceType);
        for (Object field : fields) {
            if (field != null) {
                return false;
            }
        }
        return true;
    }

    private Membership buildMembership() {
        Membership result = new Membership();
        result.setName(this.name);
        result.setLastNames(this.lastNames);
        result.setMemberId(this.memberId);
        result.setMemberAccountId(this.memberAccountId);
        result.setMembershipId(this.membershipId);
        result.setWebsite(this.website);
        result.setWarnings(this.warnings);
        result.setAutoRenewalStatus(this.autoRenewalStatus);
        result.setExpirationDate(this.expirationDate);
        result.setActivationDate(this.activationDate);
        result.setBalance(this.balance);
        result.setSourceType(this.sourceType);
        result.setMembershipType(this.membershipType);
        result.setMonthsDuration(this.monthsDuration);
        return result;
    }

    private void fillMissingInfo() {
        fillIdWhenZero();
        this.name = firstNonNull(this.name, stringPicker.pickAsciiAlphabetic(picker.pickInt(4, 10)));
        this.lastNames = firstNonNull(this.lastNames, stringPicker.pickAsciiAlphabetic(picker.pickInt(4, 10)));
        this.website = firstNonNull(this.website, PickerUtils.pickWebsite(picker));
        this.autoRenewalStatus = firstNonNull(this.autoRenewalStatus, PickerUtils.pickStatus(picker));
        this.expirationDate = firstNonNull(this.expirationDate,
                new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(picker.pickDateInTheFuture().getTime()));
        this.activationDate = firstNonNull(this.activationDate,
                new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(picker.pickDateNotAfter(Calendar.getInstance()).getTime()));
        this.membershipType = firstNonNull(this.membershipType, PickerUtils.pickMembershipType(picker));
        this.sourceType = firstNonNull(this.sourceType, PickerUtils.pickSourceType(picker));
        this.balance = firstNonNull(this.balance, picker.pickPrice(99));
        if (this.warnings == null) {
            this.warnings = new ArrayList<>();
        }
    }

    private void fillIdWhenZero() {
        if (this.memberId == 0L) {
            this.memberId = picker.pickLong(1, 100);
        }
        if (this.memberAccountId == 0L) {
            this.memberAccountId = picker.pickLong(1, 100);
        }
        if (this.membershipId == 0L) {
            this.membershipId = picker.pickLong(1, 100);
        }
        if (this.bookingIdSubscription == 0L) {
            this.bookingIdSubscription = picker.pickLong(1, 100);
        }
        if (this.monthsDuration == 0) {
            this.monthsDuration = picker.pickFrom(Arrays.asList(1, 6, 12));
        }
    }
}

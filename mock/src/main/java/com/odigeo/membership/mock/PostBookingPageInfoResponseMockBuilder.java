package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.PostBookingPageInfoResponse;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;

import static com.google.common.base.MoreObjects.firstNonNull;

public class PostBookingPageInfoResponseMockBuilder {

    public static final String NO_PARAMETERS = "No parameters set for Posbooking";
    private final Picker picker;
    private final StringPicker stringPicker;
    private long bookingId;
    private String email;

    public PostBookingPageInfoResponseMockBuilder(Random random) {
        this.picker = new Picker(random);
        this.stringPicker = new StringPicker(random);
    }

    public PostBookingPageInfoResponseMockBuilder setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public PostBookingPageInfoResponseMockBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public PostBookingPageInfoResponse build() {
        if (bookingId == 0 && StringUtils.isEmpty(email)) {
            throw new IllegalArgumentException(NO_PARAMETERS);
        }
        fillMissingInfo();
        return buildPostBookingPageInfoResponse();
    }

    private void fillMissingInfo() {
        if (bookingId == 0) {
            this.bookingId = picker.pickLong(1000, 10000);
        }
        this.email = firstNonNull(this.email, stringPicker.pickAsciiAlphabetic(picker.pickInt(8, 16)));
    }

    private PostBookingPageInfoResponse buildPostBookingPageInfoResponse() {
        return new PostBookingPageInfoResponse()
                .setBookingId(this.bookingId)
                .setEmail(this.email);
    }
}

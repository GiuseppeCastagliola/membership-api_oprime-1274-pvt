package com.odigeo.product;

import com.odigeo.product.request.auth.MembershipProductModuleCredentials;

abstract class AbstractBuilder<T extends AbstractBuilder<T>> {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAUTL_POOL_SIZE = 50;

    private int connectionTimeoutInMillis;
    private int socketTimeoutInMillis;
    private int maxConcurrentConnections;
    private MembershipProductModuleCredentials credentials;

    AbstractBuilder() {
        connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
        socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
        maxConcurrentConnections = DEFAUTL_POOL_SIZE;
    }

    public T withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
        this.connectionTimeoutInMillis = connectionTimeoutInMillis;
        return (T) this;
    }

    public T withSocketTimeoutInMillis(int socketTimeoutInMillis) {
        this.socketTimeoutInMillis = socketTimeoutInMillis;
        return (T) this;
    }

    public T withMaxConcurrentConnections(int maxConcurrentConnections) {
        this.maxConcurrentConnections = maxConcurrentConnections;
        return (T) this;
    }

    public T withCredentials(MembershipProductModuleCredentials credentials) {
        this.credentials = credentials;
        return (T) this;
    }

    public int getConnectionTimeoutInMillis() {
        return connectionTimeoutInMillis;
    }

    public int getSocketTimeoutInMillis() {
        return socketTimeoutInMillis;
    }

    public int getMaxConcurrentConnections() {
        return maxConcurrentConnections;
    }

    public MembershipProductModuleCredentials getCredentials() {
        return credentials;
    }
}

package com.odigeo.membership.exception;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeName;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("MembershipServiceException")
@JsonSubTypes({
        @JsonSubTypes.Type(
                value = MembershipBadRequestException.class,
                name = "MembershipBadRequestException"),
        @JsonSubTypes.Type(
                value = MembershipInternalServerErrorException.class,
                name = "MembershipInternalServerErrorException"),
        @JsonSubTypes.Type(
                value = MembershipNotFoundException.class,
                name = "MembershipNotFoundException"),
        @JsonSubTypes.Type(
                value = MembershipForbiddenException.class,
                name = "MembershipForbiddenException"),
        @JsonSubTypes.Type(
                value = MembershipUnauthorizedException.class,
                name = "MembershipUnauthorizedException")})
public class MembershipServiceException extends WebApplicationException {

    private final String message;
    private final Throwable cause;

    public MembershipServiceException(Response.Status status, String message, Throwable cause) {
        super(Response.status(status)
                .entity(new ExceptionBean(MembershipServiceException.class, message))
                .type(MediaType.APPLICATION_JSON).build());
        this.message = message;
        this.cause = cause;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public int getStatus() {
        return getResponse().getStatus();
    }
}

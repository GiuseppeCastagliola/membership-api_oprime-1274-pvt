package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipPreconditionFailedException extends MembershipServiceException {

    public MembershipPreconditionFailedException(String message) {
        this(message, null);
    }

    public MembershipPreconditionFailedException(String message, Throwable cause) {
        super(Response.Status.PRECONDITION_FAILED, message, cause);
    }

}

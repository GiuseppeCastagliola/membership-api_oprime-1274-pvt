package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipInternalServerErrorException extends MembershipServiceException {

    public MembershipInternalServerErrorException(String message) {
        this(message, null);
    }

    public MembershipInternalServerErrorException(String message, Throwable cause) {
        super(Response.Status.INTERNAL_SERVER_ERROR, message, cause);
    }

}

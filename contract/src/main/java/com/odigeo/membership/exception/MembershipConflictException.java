package com.odigeo.membership.exception;

import javax.ws.rs.core.Response;

public class MembershipConflictException extends MembershipServiceException {

    public MembershipConflictException(String message) {
        this(message, null);
    }

    public MembershipConflictException(String message, Throwable cause) {
        super(Response.Status.CONFLICT, message, cause);
    }

}


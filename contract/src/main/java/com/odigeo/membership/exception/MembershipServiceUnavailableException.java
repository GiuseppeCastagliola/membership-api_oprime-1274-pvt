package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipServiceUnavailableException extends MembershipServiceException {

    public MembershipServiceUnavailableException(String message) {
        this(message, null);
    }

    public MembershipServiceUnavailableException(String message, Throwable cause) {
        super(Response.Status.SERVICE_UNAVAILABLE, message, cause);
    }

}

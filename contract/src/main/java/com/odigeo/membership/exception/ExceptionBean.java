package com.odigeo.membership.exception;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
class ExceptionBean {
    private String type;
    private String message;

    ExceptionBean(Class<?> exceptionClass, String message) {
        this.type = exceptionClass.getSimpleName();
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

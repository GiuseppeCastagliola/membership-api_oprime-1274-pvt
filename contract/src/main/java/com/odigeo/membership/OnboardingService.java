package com.odigeo.membership;

import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/onboarding/v1")
@ValidateRequest
@Api(value = "Onboarding Operations", tags = "Membership user area v1")
public interface OnboardingService {

    @POST
    @Path("/onboarding")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Save an onboarding event",
            notes = "Sample request: http://serverDomain/membership/onboarding/v1/onboarding"
                    + "Sample body: "
                    + "{\"memberAccountId\": 123, \"event\": \"COMPLETED\",\"device\": \"APP\"}")
    Long saveOnboardingEvent(@Valid OnboardingEventRequest onboardingEventRequest) throws MembershipServiceException;
}

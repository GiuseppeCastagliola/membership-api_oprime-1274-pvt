package com.odigeo.membership.request.product.creation;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

public class CreatePendingToCollectRequest extends CreateMembershipRequest {

    private static final String MEMBER_ACCOUNT_ID = "memberAccountId";
    private static final String EXPIRATION_DATE = "expirationDate";
    private static final String SUBSCRIPTION_PRICE = "subscriptionPrice";
    private static final String CURRENCY_CODE = "currencyCode";
    private static final String RECURRING_ID = "recurringId";

    @NotNull
    private long memberAccountId;
    @NotNull
    private String expirationDate;
    @NotNull
    private BigDecimal subscriptionPrice;
    @NotNull
    private String currencyCode;

    private String recurringId;

    CreatePendingToCollectRequest() {
    }

    CreatePendingToCollectRequest(Builder builder) {
        super(builder);
        this.memberAccountId = builder.getMemberAccountId();
        this.expirationDate = builder.getExpirationDate();
        this.subscriptionPrice = builder.getSubscriptionPrice();
        this.currencyCode = builder.getCurrencyCode();
        this.recurringId = builder.getRecurringId();
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    @Override
    protected void populateConcreteClassFields(Map<String, String> map) {
        map.put(MEMBER_ACCOUNT_ID, String.valueOf(memberAccountId));
        map.put(EXPIRATION_DATE, expirationDate);
        map.put(SUBSCRIPTION_PRICE, subscriptionPrice.toString());
        map.put(CURRENCY_CODE, currencyCode);
        map.put(RECURRING_ID, recurringId);
    }

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder extends CreateMembershipRequest.Builder<Builder> {

        private long memberAccountId;
        private String expirationDate;
        private BigDecimal subscriptionPrice;
        private String recurringId;

        long getMemberAccountId() {
            return memberAccountId;
        }

        public Builder withMemberAccountId(long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        String getExpirationDate() {
            return expirationDate;
        }

        public Builder withExpirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        BigDecimal getSubscriptionPrice() {
            return subscriptionPrice;
        }

        public Builder withSubscriptionPrice(BigDecimal subscriptionPrice) {
            this.subscriptionPrice = subscriptionPrice;
            return this;
        }

        String getRecurringId() {
            return recurringId;
        }

        public Builder withRecurringId(String recurringId) {
            this.recurringId = recurringId;
            return this;
        }

        public CreatePendingToCollectRequest build() {
            return new CreatePendingToCollectRequest(this);
        }
    }
}

package com.odigeo.membership.request.postbooking;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PostBookingPageInfoRequest implements Serializable {
    @NotNull
    private String token;

    public String getToken() {
        return token;
    }

    public PostBookingPageInfoRequest setToken(String token) {
        this.token = token;
        return this;
    }
}

package com.odigeo.membership.request.product.creation;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.nonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "typeInfo")
@JsonSubTypes({@Type(
    value = CreateNewMembershipRequest.class,
    name = "CreateNewMembershipRequest"
    ), @Type(
    value = CreatePendingToCollectRequest.class,
    name = "CreatePendingToCollectRequest"
    ), @Type(
        value = CreateBasicFreeMembershipRequest.class,
        name = "CreateBasicFreeMembershipRequest"
    )})
public abstract class CreateMembershipRequest implements Serializable {

    private static final String WEBSITE_FIELD = "website";
    private static final String MONTHS_TO_RENEWAL = "monthsToRenewal";
    private static final String RENEWAL_PRICE = "renewalPrice";
    private static final String CURRENCY_CODE = "currencyCode";
    private static final String SOURCE_TYPE = "sourceType";
    private static final String MEMBERSHIP_TYPE = "membershipType";

    @NotNull
    private String website;
    @NotNull
    private int monthsToRenewal;
    @NotNull
    private String sourceType;
    @NotNull
    private String membershipType;

    private BigDecimal renewalPrice;

    private String currencyCode;

    CreateMembershipRequest() {
    }

    CreateMembershipRequest(Builder builder) {
        this.website = builder.getWebsite();
        this.monthsToRenewal = builder.getMonthsToRenewal();
        this.sourceType = builder.getSourceType();
        this.membershipType = builder.getMembershipType();
        this.renewalPrice = builder.getRenewalPrice();
        this.currencyCode = builder.getCurrencyCode();
    }

    public String getWebsite() {
        return website;
    }

    public int getMonthsToRenewal() {
        return monthsToRenewal;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public Map<String, String> getAsMap() {
        Map<String, String> map = new HashMap<>();
        map.put(WEBSITE_FIELD, website);
        map.put(MONTHS_TO_RENEWAL, String.valueOf(monthsToRenewal));
        map.put(SOURCE_TYPE, sourceType);
        map.put(MEMBERSHIP_TYPE, membershipType);
        if (nonNull(renewalPrice)) {
            map.put(RENEWAL_PRICE, getRenewalPrice().toString());
        }
        if (nonNull(currencyCode)) {
            map.put(CURRENCY_CODE, getCurrencyCode());
        }
        populateConcreteClassFields(map);
        return map;
    }

    protected abstract void populateConcreteClassFields(Map<String, String> map);

    public abstract static class Builder<T extends Builder<T>> {

        private String website;
        private int monthsToRenewal;
        private String sourceType;
        private String membershipType;
        private BigDecimal renewalPrice;
        private String currencyCode;

        String getWebsite() {
            return website;
        }

        public T withWebsite(String website) {
            this.website = website;
            return (T) this;
        }

        int getMonthsToRenewal() {
            return monthsToRenewal;
        }

        public T withMonthsToRenewal(int monthsToRenewal) {
            this.monthsToRenewal = monthsToRenewal;
            return (T) this;
        }

        String getSourceType() {
            return sourceType;
        }

        public T withSourceType(String sourceType) {
            this.sourceType = sourceType;
            return (T) this;
        }

        String getMembershipType() {
            return membershipType;
        }

        public T withMembershipType(String membershipType) {
            this.membershipType = membershipType;
            return (T) this;
        }

        BigDecimal getRenewalPrice() {
            return renewalPrice;
        }

        public T withRenewalPrice(BigDecimal renewalPrice) {
            this.renewalPrice = renewalPrice;
            return (T) this;
        }

        String getCurrencyCode() {
            return currencyCode;
        }

        public T withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return (T) this;
        }

        public abstract CreateMembershipRequest build();
    }
}

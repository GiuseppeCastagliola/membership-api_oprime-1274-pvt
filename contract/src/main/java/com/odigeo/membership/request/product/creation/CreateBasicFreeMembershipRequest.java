package com.odigeo.membership.request.product.creation;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateBasicFreeMembershipRequest extends CreateMembershipRequest {

    private static final String USER_ID = "userId";
    private static final String USER_CREATION_INFO = "userCreationInfo";
    private static final String NAME_FIELD = "name";
    private static final String LAST_NAMES = "lastNames";
    private static final String SUBSCRIPTION_PRICE = "subscriptionPrice";
    private static final String CHANNEL = "channel";
    private static final String CURRENCY_CODE = "currencyCode";

    private String userId;
    private UserCreationInfo userCreationInfo;
    @NotNull
    private String name;
    @NotNull
    private String lastNames;
    @NotNull
    private String channel;
    @NotNull
    private String currencyCode;

    CreateBasicFreeMembershipRequest() {
    }

    CreateBasicFreeMembershipRequest(Builder builder) {
        super(builder);
        this.userId = builder.getUserId();
        this.userCreationInfo = builder.getUserCreationInfo();
        this.name = builder.getName();
        this.lastNames = builder.getLastNames();
        this.channel = builder.getChannel();
        this.currencyCode = builder.getCurrencyCode();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserCreationInfo getUserCreationInfo() {
        return userCreationInfo;
    }

    public void setUserCreationInfo(UserCreationInfo userCreationInfo) {
        this.userCreationInfo = userCreationInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    protected void populateConcreteClassFields(Map<String, String> map) {
        map.put(NAME_FIELD, name);
        map.put(LAST_NAMES, lastNames);
        if (isNotBlank(userId)) {
            map.put(USER_ID, userId);
        }
        if (nonNull(userCreationInfo)) {
            map.put(USER_CREATION_INFO, userCreationInfo.toString());
        }
        map.put(SUBSCRIPTION_PRICE, BigDecimal.ZERO.toString());
        map.put(CHANNEL, channel);
        map.put(CURRENCY_CODE, currencyCode);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends CreateMembershipRequest.Builder<Builder> {

        private String userId;
        private UserCreationInfo userCreationInfo;
        private String name;
        private String lastNames;
        private String channel;

        String getUserId() {
            return userId;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        UserCreationInfo getUserCreationInfo() {
            return userCreationInfo;
        }

        public Builder withUserCreationInfo(UserCreationInfo userCreationInfo) {
            this.userCreationInfo = userCreationInfo;
            return this;
        }

        String getName() {
            return name;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        String getLastNames() {
            return lastNames;
        }

        public Builder withLastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        String getChannel() {
            return channel;
        }

        public Builder withChannel(String channel) {
            this.channel = channel;
            return this;
        }

        public CreateBasicFreeMembershipRequest build() {
            return new CreateBasicFreeMembershipRequest(this);
        }
    }
}

package com.odigeo.membership.request.onboarding;

public enum OnboardingDevice {

    APP, MOBILE, DESKTOP
}

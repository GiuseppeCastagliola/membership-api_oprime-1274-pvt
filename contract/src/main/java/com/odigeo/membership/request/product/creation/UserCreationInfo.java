package com.odigeo.membership.request.product.creation;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCreationInfo implements Serializable {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @NotNull
    private String email;
    @NotNull
    private String locale;
    @NotNull
    private Integer trafficInterfaceId;

    UserCreationInfo() {
    }

    UserCreationInfo(Builder builder) {
        this.email = builder.getEmail();
        this.locale = builder.getLocale();
        this.trafficInterfaceId = builder.getTrafficInterfaceId();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Integer getTrafficInterfaceId() {
        return trafficInterfaceId;
    }

    public void setTrafficInterfaceId(Integer trafficInterfaceId) {
        this.trafficInterfaceId = trafficInterfaceId;
    }

    public String toString() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (IOException e) {
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }

    public static class Builder {

        private String email;
        private String locale;
        private Integer trafficInterfaceId;

        public String getEmail() {
            return email;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public String getLocale() {
            return locale;
        }

        public Builder withLocale(String locale) {
            this.locale = locale;
            return this;
        }

        public Integer getTrafficInterfaceId() {
            return trafficInterfaceId;
        }

        public Builder withTrafficInterfaceId(Integer trafficInterfaceId) {
            this.trafficInterfaceId = trafficInterfaceId;
            return this;
        }

        public UserCreationInfo build() {
            return new UserCreationInfo(this);
        }
    }
}

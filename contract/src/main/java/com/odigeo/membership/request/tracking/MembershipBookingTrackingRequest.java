package com.odigeo.membership.request.tracking;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipBookingTrackingRequest implements Serializable {

    @NotNull
    private long bookingId;
    @NotNull
    private long membershipId;
    private String bookingDate;
    private BigDecimal avoidFeeAmount;
    private BigDecimal costFeeAmount;
    private BigDecimal perksAmount;

    public MembershipBookingTrackingRequest() {
    }

    public MembershipBookingTrackingRequest(Builder builder) {
        this.bookingId = builder.bookingId;
        this.membershipId = builder.membershipId;
        this.bookingDate = builder.bookingDate;
        this.avoidFeeAmount = builder.avoidFeeAmount;
        this.costFeeAmount = builder.costFeeAmount;
        this.perksAmount = builder.perksAmount;
    }

    public static Builder builder(long bookingId, long membershipId) {
        return new Builder(bookingId, membershipId);
    }

    public long getBookingId() {
        return bookingId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public BigDecimal getPerksAmount() {
        return perksAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipBookingTrackingRequest that = (MembershipBookingTrackingRequest) o;
        return bookingId == that.bookingId && membershipId == that.membershipId
                && Objects.equals(bookingDate, that.bookingDate) && Objects.equals(avoidFeeAmount, that.avoidFeeAmount)
                && Objects.equals(costFeeAmount, that.costFeeAmount) && Objects.equals(perksAmount, that.perksAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, membershipId, bookingDate, avoidFeeAmount, costFeeAmount, perksAmount);
    }

    public static final class Builder {
        private final long bookingId;
        private final long membershipId;
        private String bookingDate;
        private BigDecimal avoidFeeAmount;
        private BigDecimal costFeeAmount;
        private BigDecimal perksAmount;

        public Builder(long bookingId, long membershipId) {
            this.bookingId = bookingId;
            this.membershipId = membershipId;
        }

        public Builder bookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
            return this;
        }

        public Builder avoidFeeAmount(BigDecimal avoidFeeAmount) {
            this.avoidFeeAmount = avoidFeeAmount;
            return this;
        }

        public Builder costFeeAmount(BigDecimal costFeeAmount) {
            this.costFeeAmount = costFeeAmount;
            return this;
        }

        public Builder perksAmount(BigDecimal perksAmount) {
            this.perksAmount = perksAmount;
            return this;
        }

        public MembershipBookingTrackingRequest build() {
            return new MembershipBookingTrackingRequest(this);
        }
    }
}

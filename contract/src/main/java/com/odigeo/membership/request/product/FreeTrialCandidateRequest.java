package com.odigeo.membership.request.product;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class FreeTrialCandidateRequest {

    @NotNull
    private String email;
    @NotNull
    private String website;
    private String name;
    private String lastNames;

    private FreeTrialCandidateRequest() {
    }

    FreeTrialCandidateRequest(Builder builder) {
        this.email = builder.email;
        this.website = builder.website;
        this.name = builder.name;
        this.lastNames = builder.lastNames;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public String getName() {
        return name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public static class Builder {

        private String email;
        private String website;
        private String name;
        private String lastNames;

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withWebsite(String website) {
            this.website = website;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withLastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public FreeTrialCandidateRequest build() {
            return new FreeTrialCandidateRequest(this);
        }
    }
}

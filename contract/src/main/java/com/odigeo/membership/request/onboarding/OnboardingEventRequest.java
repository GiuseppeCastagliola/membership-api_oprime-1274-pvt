package com.odigeo.membership.request.onboarding;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnboardingEventRequest implements Serializable {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @NotNull
    private long memberAccountId;
    @NotNull
    private OnboardingEvent event;
    @NotNull
    private OnboardingDevice device;

    OnboardingEventRequest() {
    }

    OnboardingEventRequest(Builder builder) {
        this.memberAccountId = builder.getMemberAccountId();
        this.event = builder.getEvent();
        this.device = builder.getDevice();
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public OnboardingEvent getEvent() {
        return event;
    }

    public void setEvent(OnboardingEvent event) {
        this.event = event;
    }

    public OnboardingDevice getDevice() {
        return device;
    }

    public void setDevice(OnboardingDevice device) {
        this.device = device;
    }

    public String toString() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (IOException e) {
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }

    public static class Builder {

        private long memberAccountId;
        private OnboardingEvent event;
        private OnboardingDevice device;

        public long getMemberAccountId() {
            return memberAccountId;
        }

        public Builder withMemberAccountId(long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public OnboardingEvent getEvent() {
            return event;
        }

        public Builder withEvent(OnboardingEvent event) {
            this.event = event;
            return this;
        }

        public OnboardingDevice getDevice() {
            return device;
        }

        public Builder withDevice(OnboardingDevice device) {
            this.device = device;
            return this;
        }

        public OnboardingEventRequest build() {
            return new OnboardingEventRequest(this);
        }
    }
}

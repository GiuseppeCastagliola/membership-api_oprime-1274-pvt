package com.odigeo.membership;

import com.odigeo.membership.annotations.Authenticated;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/membership/v1")
@ValidateRequest
@Api(value = "Membership operations", tags = "Membership v1")
public interface MembershipService {

    @GET
    @Path("/{userId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find membership by userId and website. Provide website in the end of the URL like this: \";website={websiteCode}\"",
            notes = "Sample request: http://serverDomain/membership/membership/v1/686868;website=OPFR")
    Membership getMembership(
            @NotNull @PathParam("userId") Long userId,
            @NotNull @MatrixParam("website") String site) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/{userId}/all")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find all active memberships by userId",
            notes = "Sample request: http://serverDomain/membership/membership/v1/686868/all")
    WebsiteMembership getAllMembership(@NotNull @PathParam("userId") Long userId) throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/applicable")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Check if membership discount can be applied for the user with given id and name",
            notes = "Sample request: http://serverDomain/membership/membership/v1/applicable "
                    + "Request body: {\"userId\": 686868, \"site\": \"OPFR\", \"travellerContainerList\": [{\"name\": \"John\", \"lastNames\": \"Smith\"}]}")
    Boolean applyMembership(@Valid CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/disable/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change membership status to DEACTIVATED if it currently is ACTIVATED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/disable/1085")
    MembershipStatus disableMembership(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/reactivate/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change membership status to ACTIVATED if it is currently DEACTIVATED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/reactivate/2901")
    MembershipStatus reactivateMembership(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/disable-auto-renewal/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change auto renewal flag of the membership to DISABLED if it currently is ENABLED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/disable-auto-renewal/1020")
    MembershipRenewal disableAutoRenewal(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/enable-auto-renewal/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change auto renewal flag of the membership to ENABLED if it currently is DISABLED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/enable-auto-renewal/1021")
    MembershipRenewal enableAutoRenewal(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @GET
    @Path("/website/{siteId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Check if memberships are supported on given site with given interface",
            notes = "Sample request: http://serverDomain/membership/membership/v1/website/OPDE")
    Boolean isMembershipPerksActiveOn(@NotNull @PathParam("siteId") String siteId) throws InvalidParametersException;

    @POST
    @Path("/memberships")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Creates a membership",
            notes = "Sample request: http://serverDomain/membership/membership/v1/memberships"
                    + "Sample body (new subscription): {\"typeInfo\": \"CreatePendingToCollectRequest\", \"website\": \"IT\","
                    + " \"monthsToRenewal\": \"12\", \"renewalPrice\": \"49.99\", \"currencyCode\": \"EUR\", \"sourceType\": \"FUNNEL_BOOKING\", "
                    + "\"membershipType\": \"BUSINESS\", \"userId\": \"7\",\"name\": \"John\",\"lastNames\": \"Smith\"}"
                    + "Sample body (Pending to collect): {\"typeInfo\": \"CreatePendingToCollectRequest\", \"website\": \"ES\","
                    + " \"monthsToRenewal\": 6, \"renewalPrice\": \"49.99\", \"currencyCode\": \"EUR\", \"sourceType\": \"FUNNEL_BOOKING\", "
                    + "\"membershipType\": \"BUSINESS\", \"memberAccountId\": \"763\", \"expirationDate\": \"2019-02-05\","
                    + " \"subscriptionPrice\": 34.99, \"currencyCode\": \"EUR\", \"recurringId\": \"abc\"}")
    Long createMembership(@Valid CreateMembershipRequest createMembershipRequest) throws MembershipServiceException;

    @PUT
    @Path("/updateMemberAccount")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Update memberAccount by memberAccountId. Operation is needed to define operation (without it will fall to default: UPDATE_NAMES)\n"
            + "Operations available: UPDATE_NAMES, UPDATE_USER_ID")
    Boolean updateMemberAccount(@Valid UpdateMemberAccountRequest memberAccountRequest) throws MembershipServiceException;

    @PUT
    @Path("/updateMembership")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Update membership by membershipId and operation needed, available operations: REACTIVATE_MEMBERSHIP, DEACTIVATE_MEMBERSHIP,"
            + " EXPIRE_MEMBERSHIP, ENABLE_AUTO_RENEWAL, DISABLE_AUTO_RENEWAL, CONSUME_MEMBERSHIP_REMNANT_BALANCE, INSERT_RECURRING_ID ",
            notes = "This endpoint should be used in replacement of the deprecated ones (still working)")
    Boolean updateMembership(@Valid UpdateMembershipRequest membershipRequest) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/retry-membership-activation/{bookingId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Retry activating membership of given bookingId")
    Boolean retryMembershipActivation(@NotNull @PathParam("bookingId") Long bookingId) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/paymentMethods/blacklist/{id}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Return any blacklisted payment method for the given membership, if any. An empty list if none found.",
            notes = "Sample request: http://serverDomain/membership/membership/v1/paymentMethods/blacklist/686868")
    List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/paymentMethods/blacklist/{id}")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Blacklist the given payment method associated with the membership.",
            notes = "Sample request: {\"blackListedPaymentMethods\": [{\"id\": 3,\"timestamp\": 1554390401666,\"errorType\": \"errorType\",\"errorMessage\": \"errorMessage\"}]}")
    Boolean addToBlackList(@NotNull @PathParam("id") Long membershipId, @Valid AddToBlackListRequest addToBlackListRequest) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/newVatModelDate")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Returns the cutover date for the new VAT model")
    String getNewVatModelDate() throws MembershipServiceException;

    @GET
    @Path("/can-renew/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Checks if membership is active and expiration date has passed")
    Boolean isMembershipToBeRenewed(@NotNull @PathParam("membershipId") Long membershipId) throws MembershipServiceException;

    @GET
    @Path("/current")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find active or pending to collect membership by userId and website. Provide website in the end of the URL like this: \";website={websiteCode}\"",
            notes = "Sample request: http://serverDomain/membership/membership/v1/current;userId=686868;website=OPFR")
    MembershipResponse getCurrentMembership(
            @NotNull @MatrixParam("userId") Long userId,
            @NotNull @MatrixParam("website") String site) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/activate/{membershipId}")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Activate the membership identified by the path parameter membershipId, updating his balance with the amount in activation request. This endpoint needs authentication, the client should add to the MembershipService.properties in commons rest path,"
            + "the two values: client= an identifier of the client module , key= the secret key shared between membership and the module (agreed with membership and possibly put in the vault of the two modules)")
    @Authenticated
    MembershipResponse activateMembership(@NotNull @PathParam("membershipId") long membershipId, @Valid ActivationRequest activateRequest) throws MembershipServiceException;

    @POST
    @Path("/free-trial/eligible")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Check if user is eligible for a free trial",
            notes = "Sample request: {\"email\": \"customer@edreams.com\",\"website\": \"ES\",\"name\": \"Juan\",\"lastNames\": \"Silva\"}")
    Boolean isEligibleForFreeTrial(@Valid FreeTrialCandidateRequest freeTrialCandidateRequest) throws MembershipServiceException;

    @GET
    @Path("/tracked-bookings/{bookingId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Retrieve a tracked booking by bookingId")
    MembershipBookingTracking getBookingTracking(@NotNull @PathParam("bookingId") long bookingId) throws MembershipServiceException;

    @POST
    @Path("/tracked-bookings")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Insert a booking in the tracking table and update the membership balance (sum avoidFees passed), return the successfully inserted booking tracked",
            notes = "Sample request: {\"bookingId\":\"3815230851\", \"membershipId\":\"113769\",\"bookingDate\":\"2019-12-12T00:00:00\", \"avoidFeeAmount\":\"-7.240\","
                    + "\"costFeeAmount\":\"0\", \"perksAmount\":\"-20\"}")
    MembershipBookingTracking addBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) throws MembershipServiceException;

    @DELETE
    @Path("/tracked-bookings")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Delete a booking in the tracking table and update the membership balance (subtract avoidFees of the deleted booking), return 204 void if succeeds",
            notes = "Sample request: {\"bookingId\":\"3815230851\", \"membershipId\":\"113769\"}")
    void deleteBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) throws MembershipServiceException;
}

package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipRecurring implements Serializable {

    private String recurringId;
    private String status;

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipRecurring that = (MembershipRecurring) o;
        return Objects.equals(recurringId, that.recurringId)
                && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recurringId, status);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MembershipRecurring.class.getSimpleName() + "[", "]")
                .add("recurringId='" + recurringId + '\'')
                .add("status='" + status + '\'')
                .toString();
    }
}

package com.odigeo.membership.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PostBookingBatchResultResponse implements Serializable {

    private final List<String> errors;

    public PostBookingBatchResultResponse() {
        errors = new ArrayList();
    }

    public PostBookingBatchResultResponse(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String error) {
        this.errors.add(error);
    }

    public List<String> getErrors() {
        return errors;
    }

}

package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebsiteMembership implements Serializable {

    private Map<String, Membership> websiteMembershipMap;

    public Map<String, Membership> getWebsiteMembershipMap() {
        return websiteMembershipMap;
    }

    public void setWebsiteMembershipMap(Map<String, Membership> websiteMembershipMap) {
        this.websiteMembershipMap = websiteMembershipMap;
    }
}

package com.odigeo.membership.response.tracking;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipBookingTracking implements Serializable {

    private long bookingId;
    private long membershipId;
    private String bookingDate;
    private BigDecimal avoidFeeAmount;
    private BigDecimal costFeeAmount;
    private BigDecimal perksAmount;

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(long membershipId) {
        this.membershipId = membershipId;
    }

    public String getBookingDate() {
        return bookingDate;
    }


    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public void setAvoidFeeAmount(BigDecimal avoidFeeAmount) {
        this.avoidFeeAmount = avoidFeeAmount;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public void setCostFeeAmount(BigDecimal costFeeAmount) {
        this.costFeeAmount = costFeeAmount;
    }

    public BigDecimal getPerksAmount() {
        return perksAmount;
    }

    public void setPerksAmount(BigDecimal perksAmount) {
        this.perksAmount = perksAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipBookingTracking that = (MembershipBookingTracking) o;
        return bookingId == that.bookingId && membershipId == that.membershipId
                && Objects.equals(bookingDate, that.bookingDate) && Objects.equals(avoidFeeAmount, that.avoidFeeAmount)
                && Objects.equals(costFeeAmount, that.costFeeAmount) && Objects.equals(perksAmount, that.perksAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, membershipId, bookingDate, avoidFeeAmount, costFeeAmount, perksAmount);
    }
}

package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Membership extends MembershipBaseResponse implements Serializable {

}

package com.odigeo.membership.response;

public class MembershipStatus {
    private String status;

    public MembershipStatus(String status) {
        this.status = status;
    }

    public MembershipStatus() {
    }

    public String getStatus() {
        return status;
    }

    public MembershipStatus setStatus(String status) {
        this.status = status;
        return this;
    }

}

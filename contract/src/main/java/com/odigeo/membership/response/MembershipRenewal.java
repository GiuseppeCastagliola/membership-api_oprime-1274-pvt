package com.odigeo.membership.response;

public class MembershipRenewal {
    private String renewalStatus;

    public MembershipRenewal() {
    }

    public MembershipRenewal(String renewalStatus) {
        this.renewalStatus = renewalStatus;
    }

    public String getRenewalStatus() {
        return renewalStatus;
    }

    public MembershipRenewal setRenewalStatus(String renewalStatus) {
        this.renewalStatus = renewalStatus;
        return this;
    }
}

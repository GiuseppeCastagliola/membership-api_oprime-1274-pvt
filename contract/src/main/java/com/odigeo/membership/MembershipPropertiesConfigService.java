package com.odigeo.membership;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.constraints.NotNull;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/config/v1")
@ValidateRequest
@Api(value = "Membership configuration operations", tags = "Membership configuration v1")
public interface MembershipPropertiesConfigService {

    @PUT
    @Path("/property/enable/{key}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Enable configuration property",
            notes = "Sample request: http://serverDomain/membership/config/v1/property/enable/SEND_IDS_TO_KAFKA")
    Boolean enableConfigurationProperty(@NotNull @PathParam("key")String propertyKey);

    @PUT
    @Path("/property/disable/{key}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Disable configuration property",
            notes = "Sample request: http://serverDomain/membership/config/v1/property/disable/SEND_IDS_TO_KAFKA")
    Boolean disableConfigurationProperty(@NotNull @PathParam("key")String propertyKey);

}

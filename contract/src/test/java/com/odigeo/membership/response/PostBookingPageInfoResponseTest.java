package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class PostBookingPageInfoResponseTest extends BeanTest<PostBookingPageInfoResponse> {

    private static final long BOOKING_ID = 1234L;
    private static final String EMAIL = "fake@email.com";

    @Override
    protected PostBookingPageInfoResponse getBean() {
        return new PostBookingPageInfoResponse().setBookingId(BOOKING_ID).setEmail(EMAIL);
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(PostBookingPageInfoResponse.class)
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}

package com.odigeo.membership.request.product.creation;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class UserCreationInfoTest {

    private static final String EMAIL = "passenger@mail.com";
    private static final String LOCALE = "fr_FR";
    private static final Integer TRAFFIC_INTERFACE_ID = 2;

    @Test
    public void testUserCreationInfoBuilder() {
        UserCreationInfo userCreationInfo = new UserCreationInfo.Builder()
                .withEmail(EMAIL)
                .withLocale(LOCALE)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .build();
        assertEquals(EMAIL, userCreationInfo.getEmail());
        assertEquals(LOCALE, userCreationInfo.getLocale());
        assertEquals(TRAFFIC_INTERFACE_ID, userCreationInfo.getTrafficInterfaceId());
    }
}

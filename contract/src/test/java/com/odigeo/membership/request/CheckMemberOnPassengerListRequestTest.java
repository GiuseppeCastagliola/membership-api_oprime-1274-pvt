package com.odigeo.membership.request;

import com.odigeo.membership.request.container.TravellerParametersContainer;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class CheckMemberOnPassengerListRequestTest {

    @Test
    public void testCheckMemberOnPassengerListRequest() {

        CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest = new CheckMemberOnPassengerListRequest();
        checkMemberOnPassengerListRequest.setUserId(123L);
        checkMemberOnPassengerListRequest.setTravellerContainerList(new ArrayList<TravellerParametersContainer>());

        TravellerParametersContainer travellerParametersContainer = new TravellerParametersContainer();
        travellerParametersContainer.setName("Name");
        travellerParametersContainer.setLastNames("LastName");
        checkMemberOnPassengerListRequest.getTravellerContainerList().add(travellerParametersContainer);

        assertEquals(checkMemberOnPassengerListRequest.getUserId().longValue(), 123L);
        assertEquals(checkMemberOnPassengerListRequest.getTravellerContainerList().get(0).getName(), "Name");
        assertEquals(checkMemberOnPassengerListRequest.getTravellerContainerList().get(0).getLastNames(), "LastName");
    }
}

package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class CreateMembershipSubscriptionRequestTest extends BeanTest<CreateMembershipSubscriptionRequest> {

    private static final String CURRENCY_CODE = "EUR";

    @Override
    protected CreateMembershipSubscriptionRequest getBean() {
        CreateMembershipSubscriptionRequest membershipSubscriptionRequest = new CreateMembershipSubscriptionRequest();
        membershipSubscriptionRequest.setBalance(BigDecimal.ZERO);
        membershipSubscriptionRequest.setRenewalPrice(BigDecimal.TEN);
        membershipSubscriptionRequest.setCurrencyCode(CURRENCY_CODE);
        return membershipSubscriptionRequest;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

package com.odigeo.membership.request.product.creation;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CreateNewMembershipCreationTest {

    private static final String WEBSITE_FIELD = "website";
    private static final String MONTHS_TO_RENEWAL_FIELD = "monthsToRenewal";
    private static final String SOURCE_TYPE_FIELD = "sourceType";
    private static final String MEMBERSHIP_TYPE_FIELD = "membershipType";
    private static final String USER_ID_FIELD = "userId";
    private static final String USER_CREATION_INFO_FIELD = "userCreationInfo";
    private static final String NAME_FIELD = "name";
    private static final String LAST_NAMES_FIELD = "lastNames";
    private static final String SUBSCRIPTION_PRICE_FIELD = "subscriptionPrice";
    private static final int MONTHS_TO_RENEWAL = 6;
    private static final String SOURCE_TYPE = "POST_BOOKING";
    private static final String MEMBERSHIP_TYPE = "BASIC";
    private static final String USER_ID = "92";
    private static final String NAME = "Boris";
    private static final String EMAIL = "boris@pochta.ru";
    private static final String LAST_NAMES = "Morozov";
    private static final String WEBSITE = "FR";
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;

    @Test
    public void testPopulateConcreteClassFieldsMandatoryFields() {
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withWebsite(WEBSITE)
                .withMonthsToRenewal(MONTHS_TO_RENEWAL)
                .withSourceType(SOURCE_TYPE)
                .withMembershipType(MEMBERSHIP_TYPE)
                .withName(NAME)
                .withLastNames(LAST_NAMES)
                .build();

        Map<String, String> fieldsMap = request.getAsMap();
        assertEquals(fieldsMap.get(WEBSITE_FIELD), WEBSITE);
        assertEquals(fieldsMap.get(MONTHS_TO_RENEWAL_FIELD), String.valueOf(MONTHS_TO_RENEWAL));
        assertEquals(fieldsMap.get(SOURCE_TYPE_FIELD), SOURCE_TYPE);
        assertEquals(fieldsMap.get(MEMBERSHIP_TYPE_FIELD), MEMBERSHIP_TYPE);
        assertEquals(fieldsMap.get(NAME_FIELD), NAME);
        assertEquals(fieldsMap.get(LAST_NAMES_FIELD), LAST_NAMES);
    }

    @Test
    public void testPopulateConcreteClassFieldsWithUserId() {
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withUserId(USER_ID)
                .build();
        assertEquals(request.getAsMap().get(USER_ID_FIELD), USER_ID);
    }

    @Test
    public void testPopulateConcreteClassFieldsWithUserCreationInfo() {
        UserCreationInfo userCreationInfo = new UserCreationInfo.Builder().withEmail(EMAIL).build();
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withUserCreationInfo(userCreationInfo)
                .build();
        assertTrue(request.getAsMap().get(USER_CREATION_INFO_FIELD).indexOf(EMAIL) > 0);
    }

    @Test
    public void testPopulateConcreteClassFieldsWithSubscriptionPrice() {
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withSubscriptionPrice(SUBSCRIPTION_PRICE)
                .build();
        assertTrue(request.getAsMap().containsKey(SUBSCRIPTION_PRICE_FIELD));
        assertEquals(request.getAsMap().get(SUBSCRIPTION_PRICE_FIELD), String.valueOf(SUBSCRIPTION_PRICE));
    }
}

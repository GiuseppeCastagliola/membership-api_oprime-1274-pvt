package com.odigeo.membership.request.product.creation;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class CreatePendingToCollectRequestTest extends BeanTest<CreatePendingToCollectRequest> {

    private static final String EXPIRATION_DATE = "10/10/2029";
    private static final Long MEMBER_ACCOUNT_ID = 123L;
    private static final String RECURRING_ID = "1234";
    private static final String WEBSITE = "ES";
    private static final String MEMBERSHIP_TYPE = "BASIC";
    private static final String SOURCE_TYPE = "FUNNEL_BOOKING";
    private static final BigDecimal RENEWAL_PRICE = new BigDecimal(20);
    private static final BigDecimal SUBSCRIPTION_PRICE = new BigDecimal(22);
    private static final String CURRENCY_CODE = "EUR";
    private static final Integer DURATION = 1;

    @Override
    protected CreatePendingToCollectRequest getBean() {
        return CreatePendingToCollectRequest.builder()
                .withSubscriptionPrice(SUBSCRIPTION_PRICE)
                .withExpirationDate(EXPIRATION_DATE)
                .withMemberAccountId(MEMBER_ACCOUNT_ID)
                .withRecurringId(RECURRING_ID)
                .withWebsite(WEBSITE)
                .withCurrencyCode(CURRENCY_CODE)
                .withMembershipType(MEMBERSHIP_TYPE)
                .withRenewalPrice(RENEWAL_PRICE)
                .withMonthsToRenewal(DURATION)
                .withSourceType(SOURCE_TYPE)
                .build();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

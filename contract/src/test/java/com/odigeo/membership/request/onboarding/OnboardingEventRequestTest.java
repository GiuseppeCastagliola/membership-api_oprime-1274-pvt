package com.odigeo.membership.request.onboarding;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OnboardingEventRequestTest {

    @Test
    public void testBuilderCreation() {
        //Given
        long memberAccountId = 8234L;
        OnboardingDevice device = OnboardingDevice.APP;
        OnboardingEvent event = OnboardingEvent.COMPLETED;
        //When
        OnboardingEventRequest request = new OnboardingEventRequest.Builder()
                .withDevice(device)
                .withEvent(event)
                .withMemberAccountId(memberAccountId)
                .build();
        //Then
        assertEquals(request.getDevice(), device);
        assertEquals(request.getEvent(), event);
        assertEquals(request.getMemberAccountId(), memberAccountId);
    }

}

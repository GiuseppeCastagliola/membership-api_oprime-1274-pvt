package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;
import java.math.BigDecimal;

public class ActivationRequestTest  extends BeanTest<ActivationRequest> {
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final long BOOKING_ID = 888L;

    @Override
    protected ActivationRequest getBean() {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBookingId(BOOKING_ID);
        activationRequest.setBalance(BALANCE);
        return activationRequest;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

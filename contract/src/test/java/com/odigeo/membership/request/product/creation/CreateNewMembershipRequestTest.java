package com.odigeo.membership.request.product.creation;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class CreateNewMembershipRequestTest extends BeanTest<CreateNewMembershipRequest> {

    private static final String USER_ID = "1a";
    private static final String NAME = "ME";
    private static final String LAST_NAME = "YOU";
    private static final String WEBSITE = "ES";
    private static final String MEMBERSHIP_TYPE = "BASIC";
    private static final String SOURCE_TYPE = "FUNNEL_BOOKING";
    private static final BigDecimal RENEWAL_PRICE = new BigDecimal(20);
    private static final BigDecimal SUBSCRIPTION_PRICE = new BigDecimal(22);
    private static final String CURRENCY_CODE = "EUR";
    private static final Integer DURATION = 1;

    @Override
    protected CreateNewMembershipRequest getBean() {
        return CreateNewMembershipRequest.builder()
                .withUserId(USER_ID)
                .withName(NAME)
                .withLastNames(LAST_NAME)
                .withWebsite(WEBSITE)
                .withCurrencyCode(CURRENCY_CODE)
                .withMembershipType(MEMBERSHIP_TYPE)
                .withSubscriptionPrice(SUBSCRIPTION_PRICE)
                .withRenewalPrice(RENEWAL_PRICE)
                .withMonthsToRenewal(DURATION)
                .withSourceType(SOURCE_TYPE)
                .build();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

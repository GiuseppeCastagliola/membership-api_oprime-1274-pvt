package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;

public class UpdateMembershipRequestTest extends BeanTest<UpdateMembershipRequest> {

    private static final long USER_CREDIT_CARD_ID = 1234L;
    private static final String RECURRING_ID = "1234567";

    @Override
    protected UpdateMembershipRequest getBean() {
        UpdateMembershipRequest updateMembershipRequest = new UpdateMembershipRequest().setUserCreditCardId(USER_CREDIT_CARD_ID);
        updateMembershipRequest.setRecurringId(RECURRING_ID);
        return updateMembershipRequest;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

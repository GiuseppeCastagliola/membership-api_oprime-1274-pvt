package com.odigeo.product.response.exception;

public enum MembershipProductServiceExceptionType {

    BAD_REQUEST(400), FORBIDDEN(403), INTERNAL_SERVER_ERROR(500), NOT_FOUND(404), UNKNOWN(500);

    private int httpCode;

    MembershipProductServiceExceptionType(int httpCode) {
        this.httpCode = httpCode;
    }

    public int getHttpCode() {
        return httpCode;
    }
}

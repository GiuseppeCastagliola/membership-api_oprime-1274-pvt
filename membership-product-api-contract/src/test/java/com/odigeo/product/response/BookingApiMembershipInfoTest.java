package com.odigeo.product.response;

import com.odigeo.product.utils.BeanTest;

import java.math.BigDecimal;
import java.util.Date;

public class BookingApiMembershipInfoTest extends BeanTest<BookingApiMembershipInfo> {

    @Override
    protected BookingApiMembershipInfo getBean() {
        BookingApiMembershipInfo bean = new BookingApiMembershipInfo();
        bean.setRenewal(true);
        bean.setMembershipId(1L);
        bean.setPrice(BigDecimal.TEN);
        bean.setCurrencyCode("EUR");
        bean.setRenewalDate(new Date());
        return bean;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}

package com.odigeo.product.response.exception;

import com.odigeo.product.v2.model.enums.ProductType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MembershipProductServiceExceptionTest {

    @Test
    public void testException() {
        MembershipProductServiceException exception = new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST,  "foo", ProductType.MEMBERSHIP_RENEWAL, "bar");
        assertEquals(exception.getMembershipProductServiceExceptionType(), MembershipProductServiceExceptionType.BAD_REQUEST);
        assertEquals(exception.getMessage(), "[Product MEMBERSHIP_RENEWAL, id foo] bar");
    }

    @Test
    public void testExceptionWithCause() {
        Throwable cause = new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST,  "foo", ProductType.MEMBERSHIP_RENEWAL, "bar");
        MembershipProductServiceException exception = new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST,  "foo", ProductType.MEMBERSHIP_RENEWAL, "bar", cause);
        assertEquals(exception.getMembershipProductServiceExceptionType(), MembershipProductServiceExceptionType.BAD_REQUEST);
        assertEquals(exception.getMessage(), "[Product MEMBERSHIP_RENEWAL, id foo] bar");
        assertEquals(exception.getCause(), cause);
    }

}

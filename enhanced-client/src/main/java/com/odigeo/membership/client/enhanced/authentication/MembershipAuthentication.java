package com.odigeo.membership.client.enhanced.authentication;

public class MembershipAuthentication {
    private final String clientId;
    private final String secretKey;

    public MembershipAuthentication(String clientId, String secretKey) {
        this.clientId = clientId;
        this.secretKey = secretKey;
    }

    String getSecretKey() {
        return secretKey;
    }

    String getClientId() {
        return clientId;
    }
}

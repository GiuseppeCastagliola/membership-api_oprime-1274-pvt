package com.odigeo.membership.client.enhanced.authentication;

import org.apache.commons.codec.binary.Hex;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.spi.interception.ClientExecutionContext;
import org.jboss.resteasy.spi.interception.ClientExecutionInterceptor;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;

public class AuthenticationClientInterceptor implements ClientExecutionInterceptor {

    private static final String MAC_ALGORITHM = "HmacSHA256";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    private static final ZoneId ZONE_MAD = ZoneId.of("Europe/Madrid");
    private final MembershipAuthentication membershipAuthentication;

    public AuthenticationClientInterceptor(MembershipAuthentication membershipAuthentication) {
        this.membershipAuthentication = membershipAuthentication;
    }

    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    @Override
    public ClientResponse execute(ClientExecutionContext clientExecutionContext) throws Exception {
        if (nonNull(membershipAuthentication)) {
            ClientRequest request = clientExecutionContext.getRequest();
            URI uri = URI.create(request.getUri());
            String time = DATE_TIME_FORMATTER.format(ZonedDateTime.now(ZONE_MAD));
            String toHash = membershipAuthentication.getClientId() + uri.getPath() + time;
            request.header("hash", calculateHmac(toHash, membershipAuthentication.getSecretKey()));
            request.header("client", membershipAuthentication.getClientId());
        }
        return clientExecutionContext.proceed();
    }

    private String calculateHmac(String input, String key) throws InvalidKeyException, NoSuchAlgorithmException {
        Mac mac = Mac.getInstance(MAC_ALGORITHM);
        mac.init(new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), MAC_ALGORITHM));
        return Hex.encodeHexString(mac.doFinal(input.getBytes(StandardCharsets.UTF_8)));
    }

}

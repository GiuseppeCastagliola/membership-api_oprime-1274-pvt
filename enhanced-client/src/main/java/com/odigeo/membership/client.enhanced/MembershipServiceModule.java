package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.config.files.PropertiesLoader;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.client.enhanced.authentication.AuthenticationClientInterceptor;
import com.odigeo.membership.client.enhanced.authentication.MembershipAuthentication;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;

public class MembershipServiceModule extends AbstractRestUtilsModule<MembershipService> {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_POOL_SIZE = 50;
    private static final String MEMBERSHIP_SERVICE_PROPERTIES_PATH = "/com/odigeo/rest/utils/url/configuration/MembershipService.properties";

    private final ConnectionConfiguration connectionConfiguration;
    private final PropertiesLoader propertiesLoader;
    private final ConfigurationFilesManager configurationFilesManager;

    private MembershipServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(MembershipService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
        this.propertiesLoader = new PropertiesLoader();
        this.configurationFilesManager = new ConfigurationFilesManager();
    }

    @Override
    protected ServiceConfiguration<MembershipService> getServiceConfiguration(Class<MembershipService> serviceContractClass) {
        final ServiceConfiguration.Builder<MembershipService> builder = new ServiceConfiguration.Builder<>(serviceContractClass)
                .withConnectionConfiguration(connectionConfiguration);
        InterceptorConfiguration<MembershipService> membershipServiceInterceptorConfiguration = new InterceptorConfiguration<>();
        loadMembershipAuthenticationParameters()
                .map(AuthenticationClientInterceptor::new)
                .map(membershipServiceInterceptorConfiguration::addInterceptor)
                .map(builder::withInterceptorConfiguration);
        return builder.build();
    }

    private static Optional<MembershipAuthentication> parseAuthenticationProperties(Properties properties) {
        String client = properties.getProperty("client");
        String key = properties.getProperty("key");
        return StringUtils.isNoneBlank(client, key) ? Optional.of(new MembershipAuthentication(client, key)) : Optional.empty();
    }

    private Optional<MembershipAuthentication> loadMembershipAuthenticationParameters() {
        URL configurationFileUrl = configurationFilesManager.getConfigurationFileUrl(MEMBERSHIP_SERVICE_PROPERTIES_PATH);
        try {
            return Optional.ofNullable(propertiesLoader.loadPropertiesFromDisk(configurationFileUrl))
                    .flatMap(MembershipServiceModule::parseAuthenticationProperties);
        } catch (IOException e) {
            return Optional.empty();
        }
    }


    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAULT_POOL_SIZE;
        }

        public Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public MembershipServiceModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new MembershipServiceModule(connectionConfiguration, serviceNotificators);
        }

    }
}

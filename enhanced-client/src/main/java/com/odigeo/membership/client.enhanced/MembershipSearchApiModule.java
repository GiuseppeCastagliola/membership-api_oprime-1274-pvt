package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;

public class MembershipSearchApiModule extends AbstractRestUtilsModule<MembershipSearchApi> {

    private static final int DEFAULT_SEARCH_API_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SEARCH_API_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SEARCH_API_POOL_SIZE = 60;

    private final ConnectionConfiguration connectionConfiguration;

    private MembershipSearchApiModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(MembershipSearchApi.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<MembershipSearchApi> getServiceConfiguration(Class<MembershipSearchApi> serviceContractClass) {
        final ServiceConfiguration.Builder<MembershipSearchApi> builder = new ServiceConfiguration.Builder<MembershipSearchApi>(serviceContractClass).
                withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_SEARCH_API_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SEARCH_API_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAULT_SEARCH_API_POOL_SIZE;
        }

        public Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public MembershipSearchApiModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new MembershipSearchApiModule(connectionConfiguration, serviceNotificators);
        }

    }
}

package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.PostBookingMemberService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;

public class PostBookingMembershipServiceModule extends AbstractRestUtilsModule<PostBookingMemberService> {
    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_POOL_SIZE = 50;

    private final ConnectionConfiguration connectionConfiguration;

    private PostBookingMembershipServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(PostBookingMemberService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<PostBookingMemberService> getServiceConfiguration(Class<PostBookingMemberService> serviceContractClass) {
        return new ServiceConfiguration.Builder<PostBookingMemberService>(serviceContractClass)
                .withConnectionConfiguration(connectionConfiguration)
                .build();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAULT_POOL_SIZE;
        }

        public PostBookingMembershipServiceModule.Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public PostBookingMembershipServiceModule.Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public PostBookingMembershipServiceModule.Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public PostBookingMembershipServiceModule.Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public PostBookingMembershipServiceModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new PostBookingMembershipServiceModule(connectionConfiguration, serviceNotificators);
        }

    }
}

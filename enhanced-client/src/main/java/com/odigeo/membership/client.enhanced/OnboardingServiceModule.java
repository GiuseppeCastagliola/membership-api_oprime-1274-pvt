package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.OnboardingService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;

public class OnboardingServiceModule extends AbstractRestUtilsModule<OnboardingService> {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_POOL_SIZE = 50;
    private final ConnectionConfiguration connectionConfiguration;

    private OnboardingServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(OnboardingService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<OnboardingService> getServiceConfiguration(Class<OnboardingService> serviceContractClass) {
        final ServiceConfiguration.Builder<OnboardingService> builder = new ServiceConfiguration.Builder<>(serviceContractClass).
                withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }

    @SuppressWarnings("PMD")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAULT_POOL_SIZE;
        }

        public Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public OnboardingServiceModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new OnboardingServiceModule(connectionConfiguration, serviceNotificators);
        }
    }
}

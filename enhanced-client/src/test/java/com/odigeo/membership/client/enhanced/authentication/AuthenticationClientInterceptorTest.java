package com.odigeo.membership.client.enhanced.authentication;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.spi.interception.ClientExecutionContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AuthenticationClientInterceptorTest {
    private static final String KEY = "key";
    private static final String CLIENT = "client";
    private static final String TEST_URI = "http://localhost:8080/testUri";
    @Mock
    private ClientExecutionContext clientExecutionContext;
    @Mock
    private MembershipAuthentication membershipAuthentication;
    @Mock
    private ClientRequest clientRequest;
    private AuthenticationClientInterceptor authenticationClientInterceptor;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipAuthentication = new MembershipAuthentication(CLIENT, KEY);
        authenticationClientInterceptor = new AuthenticationClientInterceptor(membershipAuthentication);
    }

    @Test
    public void testExecuteWithoutAuthentication() throws Exception {
        AuthenticationClientInterceptor interceptorWithoutAuthentication = new AuthenticationClientInterceptor(null);
        interceptorWithoutAuthentication.execute(clientExecutionContext);
        verify(clientExecutionContext).proceed();
    }

    @Test
    public void testExecuteWithAuthentication() throws Exception {
        when(clientExecutionContext.getRequest()).thenReturn(clientRequest);
        when(clientRequest.getUri()).thenReturn(TEST_URI);
        authenticationClientInterceptor.execute(clientExecutionContext);
        verify(clientRequest).header(eq("hash"), any());
        verify(clientRequest).header(eq("client"), eq(CLIENT));
        verify(clientExecutionContext).proceed();
    }
}

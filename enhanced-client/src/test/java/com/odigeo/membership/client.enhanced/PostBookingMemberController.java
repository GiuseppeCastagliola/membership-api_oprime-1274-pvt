package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.PostBookingMemberService;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.postbooking.PostBookingPageInfoRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.response.PostBookingBatchResultResponse;
import com.odigeo.membership.response.PostBookingPageInfoResponse;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

public class PostBookingMemberController implements PostBookingMemberService {

    private static final String STUB_EXCEPTION_MESSAGE = "Not needed. This is just a test Stub";

    @Override
    public PostBookingBatchResultResponse processPostBookingCreation(MultipartFormDataInput input)
            throws MembershipServiceException {
        throw new UnsupportedOperationException(STUB_EXCEPTION_MESSAGE);
    }

    @Override
    public Long createPostBookingMembership(CreateMembershipSubscriptionRequest createMembershipSubscriptionRequest)
            throws MembershipServiceException {
        throw new UnsupportedOperationException(STUB_EXCEPTION_MESSAGE);
    }

    @Override
    public Long createPostBookingPendingMembership(CreateMembershipSubscriptionRequest createMembershipSubscriptionRequest) throws InvalidParametersException, MembershipServiceException {
        throw new UnsupportedOperationException(STUB_EXCEPTION_MESSAGE);
    }

    @Override
    public PostBookingPageInfoResponse getPostBookingPageInfo(PostBookingPageInfoRequest token) throws MembershipServiceException {
        throw new UnsupportedOperationException(STUB_EXCEPTION_MESSAGE);
    }
}

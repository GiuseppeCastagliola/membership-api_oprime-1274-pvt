package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.OnboardingService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;

public class OnboardingController implements OnboardingService {

    static final Long EXPECTED_SAVE_ONBOARDING_RESULT = 9374L;

    @Override
    public Long saveOnboardingEvent(OnboardingEventRequest onboardingEventRequest) throws MembershipServiceException {
        return EXPECTED_SAVE_ONBOARDING_RESULT;
    }
}

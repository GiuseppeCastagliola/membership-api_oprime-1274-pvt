package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.MembershipBackOfficeService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.backoffice.WelcomeEmailRequest;

public class MembershipBackOfficeController implements MembershipBackOfficeService {

    @Override
    public Boolean updateMembershipMarketingInfo(Long membershipId, String email)
            throws MembershipServiceException {
        return true;
    }

    @Override
    public Boolean sendWelcomeEmail(Long membershipId, WelcomeEmailRequest welcomeEmailRequest)
            throws MembershipServiceException {
        return true;
    }
}

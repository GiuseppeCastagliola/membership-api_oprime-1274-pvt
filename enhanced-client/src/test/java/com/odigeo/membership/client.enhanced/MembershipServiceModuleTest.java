package com.odigeo.membership.client.enhanced;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import com.odigeo.membership.request.product.ActivationRequest;
import org.jboss.resteasy.spi.BadRequestException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipServiceModuleTest extends ModuleTest {

    private static final String TEST_N = "testN";
    private static final String TEST_P = "testP";
    private static final String SITE = "site";
    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private MembershipServiceModule membershipServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        membershipServiceModule = new MembershipServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(membershipServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() throws InvalidParametersException {
        MembershipService membershipService = ConfigurationEngine.getInstance(MembershipService.class);
        Assert.assertTrue(membershipService.isMembershipPerksActiveOn(SITE));
        assertEquals(membershipServiceModule
                .getServiceConfiguration(MembershipService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }

    @Test
    public void testAuthenticatedEndpointModuleConfigurationAndAvailability() throws InvalidParametersException {
        MembershipService membershipService = ConfigurationEngine.getInstance(MembershipService.class);
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(BigDecimal.ONE);
        activationRequest.setBookingId(798L);
        assertNull(membershipService.activateMembership(1L, activationRequest));
        assertEquals(membershipServiceModule
                .getServiceConfiguration(MembershipService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }

    @Test(expectedExceptions = BadRequestException.class)
    public void testErrorThrownWhenNoBalanceInActivationRequest() {
        MembershipService membershipService = ConfigurationEngine.getInstance(MembershipService.class);
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBookingId(9L);
        membershipService.activateMembership(1L, activationRequest);
    }

    @Test(expectedExceptions = BadRequestException.class)
    public void testErrorThrownWhenNoBookingIdInActivationRequest() {
        MembershipService membershipService = ConfigurationEngine.getInstance(MembershipService.class);
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(BigDecimal.ONE);
        membershipService.activateMembership(1L, activationRequest);
    }
}

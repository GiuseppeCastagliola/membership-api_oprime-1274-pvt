package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.MemberUserArea;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.response.MemberSubscriptionDetails;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.response.MembershipInfo;

import java.util.List;

public class MemberUserAreaController implements MemberUserArea {

    private static final UnsupportedOperationException UNSUPPORTED_OPERATION_EXCEPTION = new UnsupportedOperationException("Not needed. This is just a test Stub");

    @Override
    public List<MemberSubscriptionDetails> getAllMemberSubscriptionsDetails(Long memberAccountId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipInfo getMembershipInfo(final Long membershipId, final Boolean skipBooking) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public long getUserId(Long membershipId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public boolean userHasMembershipForBrand(long userId, String brandCode) throws MembershipServiceException {
        return true;
    }

    @Override
    public long getMembershipAccountId(Long membershipId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipAccountInfo getMembershipAccount(final Long membershipAccountId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }
}

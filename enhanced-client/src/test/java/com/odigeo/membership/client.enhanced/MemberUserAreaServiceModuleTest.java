package com.odigeo.membership.client.enhanced;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberUserArea;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MemberUserAreaServiceModuleTest extends ModuleTest {

    private static final String TEST_N = "testN";
    private static final String TEST_P = "testP";
    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private MemberUserAreaServiceModule memberUserAreaServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        memberUserAreaServiceModule = new MemberUserAreaServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(memberUserAreaServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() {
        MemberUserArea memberUserArea = ConfigurationEngine.getInstance(MemberUserArea.class);
        Assert.assertTrue(memberUserArea.userHasMembershipForBrand(1L, "ED"));
        Assert.assertEquals(memberUserAreaServiceModule
                .getServiceConfiguration(MemberUserArea.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }
}

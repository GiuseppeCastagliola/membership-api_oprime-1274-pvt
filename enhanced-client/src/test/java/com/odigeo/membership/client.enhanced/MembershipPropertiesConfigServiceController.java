package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.MembershipPropertiesConfigService;

public class MembershipPropertiesConfigServiceController implements MembershipPropertiesConfigService {

    @Override
    public Boolean enableConfigurationProperty(final String propertyKey) {
        return true;
    }

    @Override
    public Boolean disableConfigurationProperty(final String propertyKey) {
        throw new UnsupportedOperationException();
    }
}

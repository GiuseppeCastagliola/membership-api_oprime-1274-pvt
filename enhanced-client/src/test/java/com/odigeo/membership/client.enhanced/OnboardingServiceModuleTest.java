package com.odigeo.membership.client.enhanced;


import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.OnboardingService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import com.odigeo.membership.request.onboarding.OnboardingDevice;
import com.odigeo.membership.request.onboarding.OnboardingEvent;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OnboardingServiceModuleTest extends ModuleTest {

    private static final String TEST_N = "testN";
    private static final String TEST_P = "testP";
    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private OnboardingServiceModule onboardingServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        onboardingServiceModule = new OnboardingServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(onboardingServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() {
        OnboardingService onboardingService = ConfigurationEngine.getInstance(OnboardingService.class);
        assertEquals(onboardingService.saveOnboardingEvent(new OnboardingEventRequest.Builder()
                .withMemberAccountId(1L)
                .withEvent(OnboardingEvent.COMPLETED)
                .withDevice(OnboardingDevice.APP).build()), OnboardingController.EXPECTED_SAVE_ONBOARDING_RESULT);
        assertEquals(onboardingServiceModule
                .getServiceConfiguration(OnboardingService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }

}

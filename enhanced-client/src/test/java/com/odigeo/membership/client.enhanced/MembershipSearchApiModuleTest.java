package com.odigeo.membership.client.enhanced;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MembershipSearchApiModuleTest extends ModuleTest {
    public static final String TEST_N = "testN";
    public static final String TEST_P = "testP";
    public static final String SITE = "site";
    public static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    public static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private MembershipSearchApiModule membershipSearchApiModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        membershipSearchApiModule = new MembershipSearchApiModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(membershipSearchApiModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() throws InvalidParametersException {
        MembershipSearchApi membershipSearchApi = ConfigurationEngine.getInstance(MembershipSearchApi.class);
        Assert.assertNotNull(membershipSearchApi.getMemberAccount(1L, true));
        Assert.assertEquals(membershipSearchApiModule
                .getServiceConfiguration(MembershipSearchApi.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }

}
